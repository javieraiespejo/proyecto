const electron = require ('electron')
const {app, BrowserWindow} = electron
const path = require('path')
const url = require('url')

require('electron-reload')(__dirname, {
    
})

let ventana

function createWindow() {
    ventana = new BrowserWindow ({})
    ventana.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file',
        slashes: true
    }))   
}

app.on('ready', createWindow)